<?php

use yii\db\Migration;

class m170522_151650_update_table_customers1 extends Migration
{
    public function up()
    {
		$this->addColumn('customer','description', $this->text());
    }

    public function down()
    {
        $this->dropColumn('customer','description');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
