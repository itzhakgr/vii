<?php

use yii\db\Migration;

class m170522_145345_create_table_customers extends Migration
{
    public function up()
    {
		$this->createTable('customer', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'email' => 'text',
        ));
    }

    public function down()
    {
        echo "m170522_145345_create_table_customers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
